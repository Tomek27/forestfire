// Entry point for the console program
// @author: Tomasz Brewka
#include "cuda_runtime.h"
#include "curand.h"
#include "curand_kernel.h"
#include "device_launch_parameters.h"

#include <chrono>
#include <iostream>

#include "forestfire.h"

#define BLOCK_X 32
#define BLOCK_Y 32

__global__ void forestFireCUDA(forestfire::Field *curr_generation, forestfire::Field *next_generation, int *width_forest, int *height_forest, double *ignition, double *growth, curandState *curand_states)
{
	int w = blockIdx.x * blockDim.x + threadIdx.x;
	int h = blockIdx.y * blockDim.y + threadIdx.y;

	// Check for forest boundaries
	if ((w >= *width_forest) || (h >= *height_forest)) {
		return;
	}

	// Mapping of the 2D-indexes to 1D-index
	int idx = h * (*width_forest) + w;

	unsigned int trees = 0;
	unsigned int burning = 0;
	
	// Get neighbors of current field
	// Count burning and not burning trees
	// +-------+
	// |NW|N|NE|
	// +--+-+--+
	// |W |.| E|
	// +--+-+--+
	// |SW|S|SE|
	// +-------+
	int neighbor_idx = 0;

	// Neighbor North-West
	int neighbor_w = w - 1;
	int neighbor_h = h - 1;
	if (neighbor_w >= 0 && neighbor_h >= 0) {
		neighbor_idx = neighbor_h * (*width_forest) + neighbor_w;
		switch (curr_generation[neighbor_idx]) {
		case forestfire::Field::Tree:
			trees++;
			break;
		case forestfire::Field::Burning:
			burning++;
			break;
		default:;
		}
	}

	// Neighbor North
	neighbor_w = w;
	neighbor_h = h - 1;
	if (neighbor_h >= 0) {
		neighbor_idx = neighbor_h * (*width_forest) + neighbor_w;
		switch (curr_generation[neighbor_idx]) {
		case forestfire::Field::Tree:
			trees++;
			break;
		case forestfire::Field::Burning:
			burning++;
			break;
		default:;
		}
	}

	// Neighbor North-East
	neighbor_w = w + 1;
	neighbor_h = h - 1;
	if (neighbor_w < *width_forest && neighbor_h >= 0) {
		neighbor_idx = neighbor_h * (*width_forest) + neighbor_w;
		switch (curr_generation[neighbor_idx]) {
		case forestfire::Field::Tree:
			trees++;
			break;
		case forestfire::Field::Burning:
			burning++;
			break;
		default:;
		}
	}

	// Neighbor West
	neighbor_w = w - 1;
	neighbor_h = h;
	if (neighbor_w >= 0) {
		neighbor_idx = neighbor_h * (*width_forest) + neighbor_w;
		switch (curr_generation[neighbor_idx]) {
		case forestfire::Field::Tree:
			trees++;
			break;
		case forestfire::Field::Burning:
			burning++;
			break;
		default:;
		}
	}

	// Neighbor East
	neighbor_w = w + 1;
	neighbor_h = h;
	if (neighbor_w < *width_forest) {
		neighbor_idx = neighbor_h * (*width_forest) + neighbor_w;
		switch (curr_generation[neighbor_idx]) {
		case forestfire::Field::Tree:
			trees++;
			break;
		case forestfire::Field::Burning:
			burning++;
			break;
		default:;
		}
	}

	// Neighbor South-West
	neighbor_w = w - 1;
	neighbor_h = h + 1;
	if (neighbor_w >= 0 && neighbor_h < *height_forest) {
		neighbor_idx = neighbor_h * (*width_forest) + neighbor_w;
		switch (curr_generation[neighbor_idx]) {
		case forestfire::Field::Tree:
			trees++;
			break;
		case forestfire::Field::Burning:
			burning++;
			break;
		default:;
		}
	}

	// Neighbor South
	neighbor_w = w;
	neighbor_h = h + 1;
	if (neighbor_h < *height_forest) {
		neighbor_idx = neighbor_h * (*width_forest) + neighbor_w;
		switch (curr_generation[neighbor_idx]) {
		case forestfire::Field::Tree:
			trees++;
			break;
		case forestfire::Field::Burning:
			burning++;
			break;
		default:;
		}
	}

	// Neighbor South-East
	neighbor_w = w + 1;
	neighbor_h = h + 1;
	if (neighbor_w < *width_forest && neighbor_h < *height_forest) {
		neighbor_idx = neighbor_h * (*width_forest) + neighbor_w;
		switch (curr_generation[neighbor_idx]) {
		case forestfire::Field::Tree:
			trees++;
			break;
		case forestfire::Field::Burning:
			burning++;
			break;
		default:;
		}
	}

	// CuRand: One random state per thread in block
	int rand_idx = idx % (BLOCK_X * BLOCK_Y);
	curandState local_state = curand_states[rand_idx];
	double random = curand_uniform_double(&local_state);
	curand_states[rand_idx] = local_state;

	// Define next_generation for current field
	switch (curr_generation[idx]) {
	case forestfire::Field::Tree:
		// 1. A tree that is next to a burning tree will turn into a burning tree itself
		if (burning > 0) {
			next_generation[idx] = forestfire::Field::Burning;
		}
		// 3. A tree with no burning neighbors will ignite with probability IGNITION
		//    as a result of a lightning strike.
		else {
			
			if (random < *ignition) {
				next_generation[idx] = forestfire::Field::Burning;
			}
			else {
				next_generation[idx] = forestfire::Field::Tree;
			}
		}
		break;
	case forestfire::Field::Burning:
		// 2. A burning tree will turn into an empty space
		next_generation[idx] = forestfire::Field::Empty;
		break;
	case forestfire::Field::Empty:
	{
									 // 4. An empty space will turn into a tree with probability GROWTH * (trees + 1),
									 //    where `trees` is the number of non-burning trees neighboring this space.
									 
									 if (random < (*growth * (trees + 1))) {
										 next_generation[idx] = forestfire::Field::Tree;
									 }
									 else {
										 next_generation[idx] = forestfire::Field::Empty;
									 }
									 break;
	}
	default:;
	} // switch next_generation
} // forestFireCUDA

__global__ void initCuRand(curandState *curand_states, unsigned long seed)
{
	curand_init(seed, threadIdx.x, 0, &curand_states[threadIdx.x]);
}

int main(int argc, char **argv)
{
	if (argc < 5) {
		std::cout << "Four parameters needed:" << std::endl;
		std::cout << "- Path to the text file" << std::endl;
		std::cout << "- Ignition probability (0..1)" << std::endl;
		std::cout << "- Growth probability (0..1)" << std::endl;
		std::cout << "- Number of generations (> 0)" << std::endl;
		return EXIT_FAILURE;
	}

	std::cout << "Given parameters:" << std::endl;
	for (int i = 0; i < argc; i++) {
		std::cout << i << ": " << argv[i] << std::endl;
	}

	const double IGNITION = std::stod(argv[2]);
	const double GROWTH = std::stod(argv[3]);
	const unsigned int NUM_GENERATIONS = std::stoi(argv[4]);

	// Load initial forest from the text file
	std::string path = std::string(argv[1]);
	forestfire::Forest FOREST { 0, 0, forestfire::FieldVector() };
	forestfire::load_text_file(path, FOREST);
	std::cout << "Forest: [" << FOREST.width << "x" << FOREST.height << "]" << std::endl;

	const int WIDTH = FOREST.width;
	const int HEIGHT = FOREST.height;
	const int NUM_FIELDS = WIDTH * HEIGHT;

	// Using a 1D-array to represent the 2D-forest
	forestfire::Field *curr_generation = new forestfire::Field [NUM_FIELDS];
	forestfire::Field *next_generation = new forestfire::Field [NUM_FIELDS];

	// Init current generation
	// Fill up next generation with empty space
	for (int i = 0; i < NUM_FIELDS; i++) {
		curr_generation[i] = FOREST.fields.at(i);
		next_generation[i] = forestfire::Field::Empty;
	}

	// Device Resources
	forestfire::Field *d_curr_generation;
	forestfire::Field *d_next_generation;
	int *d_width_forest;
	int *d_height_forest;
	double *d_ignition, *d_growth;
	double *d_randoms_ignition, *d_randoms_growth;

	// Allocate memory on GPU device
	const size_t FOREST_BYTES = sizeof(forestfire::Field) * NUM_FIELDS;
	cudaMalloc(&d_curr_generation, FOREST_BYTES);
	cudaMalloc(&d_next_generation, FOREST_BYTES);
	cudaMalloc(&d_width_forest, sizeof(int));
	cudaMalloc(&d_height_forest, sizeof(int));
	cudaMalloc(&d_ignition, sizeof(double));
	cudaMalloc(&d_growth, sizeof(double));
	cudaMalloc(&d_randoms_ignition, sizeof(double) * NUM_FIELDS);
	cudaMalloc(&d_randoms_growth, sizeof(double) * NUM_FIELDS);

	// Transfer data to GPU device
	cudaMemcpy(d_curr_generation, curr_generation, FOREST_BYTES, cudaMemcpyHostToDevice);
	cudaMemcpy(d_next_generation, next_generation, FOREST_BYTES, cudaMemcpyHostToDevice);
	cudaMemcpy(d_width_forest, &WIDTH, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_height_forest, &HEIGHT, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_ignition, &IGNITION, sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_growth, &GROWTH, sizeof(double), cudaMemcpyHostToDevice);

	// Define Block and Grid
	dim3 block(BLOCK_X, BLOCK_Y, 1);
	dim3 grid(
		(WIDTH % block.x == 0) ? (WIDTH / block.x) : (WIDTH / block.x + 1),
		(HEIGHT % block.y == 0) ? (HEIGHT / block.y) : (WIDTH / block.y + 1),
		1
	);

	// GPU Time measurment with CUDA Event
	cudaEvent_t start_cuda, end_cuda;
	cudaEventCreate(&start_cuda);
	cudaEventCreate(&end_cuda);

	// GPU Random
	curandState *d_curand_states;
	cudaMalloc(&d_curand_states, NUM_FIELDS * sizeof(curandState));

	//------------------------------START------------------------------
	std::cout << "Calculating..." << std::endl;
	auto start_time = std::chrono::high_resolution_clock::now();

	cudaEventRecord(start_cuda);
	initCuRand << < 1, block >> > (d_curand_states, time(NULL));
	cudaDeviceSynchronize();
	for (unsigned int generation = 1; generation <= NUM_GENERATIONS; generation++) {
		forestFireCUDA <<< grid, block >>> (d_curr_generation, d_next_generation, d_width_forest, d_height_forest, d_ignition, d_growth, d_curand_states);
		cudaDeviceSynchronize();
		std::swap(d_curr_generation, d_next_generation);
	}
	cudaEventRecord(end_cuda);

	//-------------------------------END-------------------------------
	// Calculate and print execution time
	auto end_time = std::chrono::high_resolution_clock::now();
	auto duration_milisec = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count();
	auto duration_seconds = std::chrono::duration_cast<std::chrono::seconds>(end_time - start_time).count();
	std::cout << "---------------------------------------" << std::endl;
	std::cout << "Duration: " << std::endl;
	std::cout << duration_milisec << " ms" << std::endl;
	std::cout << duration_seconds << " s" << std::endl;

	// Copy resources from GPU device
	cudaMemcpy(curr_generation, d_curr_generation, FOREST_BYTES, cudaMemcpyDeviceToHost);

	cudaEventSynchronize(end_cuda);

	// GPU Time
	std::cout << "GPU Time:" << std::endl;
	float cuda_millisec = 0.0;
	cudaEventElapsedTime(&cuda_millisec, start_cuda, end_cuda);
	std::cout << cuda_millisec << " ms" << std::endl;

	// Free GPU resources
	cudaFree(d_curr_generation);
	cudaFree(d_next_generation);
	cudaFree(d_width_forest);
	cudaFree(d_height_forest);
	cudaFree(d_ignition);
	cudaFree(d_growth);

	// Generate new path for result file
	std::string new_file_name = forestfire::generate_result_path(path, NUM_GENERATIONS);
	// Write solution to a file
	forestfire::write_text_file_1d(new_file_name, curr_generation, WIDTH, HEIGHT);

    return EXIT_SUCCESS;
}

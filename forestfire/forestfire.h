#pragma once

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

namespace forestfire 
{
	const int RANDOM_MIN = 0;
	const int RANDOM_MAX = 1;

	enum Field
	{
		Empty, Tree, Burning
	};

	typedef std::vector<Field> FieldVector;

	struct Forest
	{
		unsigned int width;
		unsigned int height;
		FieldVector fields;
	};

	void load_text_file(std::string &path, Forest &forest);

	void write_text_file(std::string &path, Field **forest, const int &width,  const int &height);

	void write_text_file_1d(std::string &path, Field *forest, const int &width, const int &height);

	void print_generation(Field **generation, const int &width, const int &height);

	std::string generate_result_path(std::string &input_path, const unsigned int &num_generations);
}

#include "forestfire.h"

void forestfire::load_text_file(std::string &path, Forest &forest)
{
	std::string line;

	unsigned int count = 0;
	unsigned int width = 0;
	unsigned int height = 0;

	std::ifstream myfile(path, std::fstream::in);

	if (myfile.is_open()) {
		while (std::getline(myfile, line)) {
			height++;
			for (auto c : line) {
				switch (c) {
				case 'T':
					forest.fields.push_back(forestfire::Field::Tree);
					count++;
					break;
				case 'X':
					forest.fields.push_back(forestfire::Field::Burning);
					count++;
					break;
				case ' ':
					forest.fields.push_back(forestfire::Field::Empty);
					count++;
					break;
				default: ;
				}
			}
		}
		width = count / height;
		forest.width = width;
		forest.height = height;

		myfile.close();
	}
	else {
		std::cerr << "Couldn't open file at: " << path << std::endl;
		exit(EXIT_FAILURE);
	}
}

void forestfire::write_text_file(std::string &path, forestfire::Field **forest, const int &width, const int &height)
{
	std::ofstream myfile(path, std::fstream::out);

	if (myfile.is_open()) {
		for (int h = 0; h < height; h++) {
			for (int w = 0; w < width; w++) {
				switch (forest[w][h]) {
				case forestfire::Field::Tree:
					myfile << "T";
					break;
				case forestfire::Field::Burning:
					myfile << "X";
					break;
				case forestfire::Field::Empty:
					myfile << " ";
					break;
				default: ;
				}
			}
			myfile << std::endl;
		}

		myfile.close();
	}
	else {
		std::cerr << "Couldn't open file at: " << path << std::endl;
		exit(EXIT_FAILURE);
	}
}

void forestfire::write_text_file_1d(std::string &path, forestfire::Field *forest, const int &width, const int &height)
{
	std::ofstream myfile(path, std::fstream::out);

	if (myfile.is_open()) {
		for (int h = 0; h < height; h++) {
			for (int w = 0; w < width; w++) {
				switch (forest[h * width + w]) {
				case forestfire::Field::Tree:
					myfile << "T";
					break;
				case forestfire::Field::Burning:
					myfile << "X";
					break;
				case forestfire::Field::Empty:
					myfile << " ";
					break;
				default:;
				}
			}
			myfile << std::endl;
		}

		myfile.close();
	}
	else {
		std::cerr << "Couldn't open file at: " << path << std::endl;
		exit(EXIT_FAILURE);
	}
}

void forestfire::print_generation(forestfire::Field **generation, const int &width, const int &height)
{
	for (int h = 0; h < height; h++) {
		for (int w = 0; w < width; w++) {
			switch (generation[w][h]) {
			case forestfire::Field::Tree:
				std::cout << "T";
				break;
			case forestfire::Field::Burning:
				std::cout << "X";
				break;
			case forestfire::Field::Empty:
				std::cout << "_";
				break;
			default: ;
			}
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

std::string forestfire::generate_result_path(std::string &input_path, const unsigned int &num_generations)
{
	auto last_backspace = input_path.rfind("\\");
	std::string path_prefix = input_path.substr(0, last_backspace + 1);
	std::string file_name = input_path.substr(last_backspace + 1, input_path.size());
	auto last_dot = file_name.rfind(".");
	std::string file_name_no_extension = file_name.substr(0, last_dot);
	std::string file_type = file_name.substr(last_dot + 1, file_name.size());
	std::string new_file_name = path_prefix + file_name_no_extension + "_result_" + std::to_string(num_generations) + "_generations." + file_type;

	return new_file_name;
}

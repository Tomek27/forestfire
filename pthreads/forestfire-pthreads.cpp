// forestfirePthreads.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
// @author: Tomasz Brewka

#include <chrono>
#include <iostream>
#include <random>

#include <pthread.h>

#include "forestfire.h"

namespace forest_thread {
	struct args
	{
		forestfire::Field **curr_generation = NULL;
		forestfire::Field **next_generation = NULL;
		int width_forest = 0;
		int height_forest = 0;
		double ignition = 0.0;
		double growth = 0.0;
		// Width interval: [width_from, width_to) = {width_from <= w < width_to}
		int width_from = 0;
		int height_from = 0;
		unsigned int num_fields = 0;
		unsigned int num_generations = 0;
	};

	pthread_barrier_t start_barrier;
	pthread_barrier_t before_swap_barrier;
	pthread_barrier_t after_swap_barrier;

	void* forestfire_pthread(void *arg)
	{
		struct args *args = (struct args*)arg;

		// Random settings
		std::random_device rd;  // Will be used to obtain a seed for the random number engine
		std::mt19937 generator(rd()); // Standard mersenne_twister_engine seeded with rd()
		std::uniform_real_distribution<> random(forestfire::RANDOM_MIN, forestfire::RANDOM_MAX);

		pthread_barrier_wait(&forest_thread::start_barrier);

		for (unsigned int generation = 1; generation <= args->num_generations; generation++) {
			unsigned int fields = 1;

			for (int h = args->height_from; (h < args->height_forest) && (fields <= args->num_fields); h++) {
				// Start position of width is depending on the position 
				// of the last field of previous thread
				int w = (h == args->height_from) ? args->width_from : 0;

				for (; (w < args->width_forest) && (fields <= args->num_fields); w++, fields++) {
				
					unsigned int trees = 0;
					unsigned int burning = 0;

					// Get neighbors of current field
					// Count burning and not burning trees
					// +-------+
					// |NW|N|NE|
					// +--+-+--+
					// |W |.| E|
					// +--+-+--+
					// |SW|S|SE|
					// +-------+

					// Neighbor North-West
					if (w - 1 >= 0 && h - 1 >= 0) {
						switch (args->curr_generation[w - 1][h - 1]) {
						case forestfire::Field::Tree:
							trees++;
							break;
						case forestfire::Field::Burning:
							burning++;
							break;
						default:;
						}
					}

					// Neighbor North
					if (h - 1 >= 0) {
						switch (args->curr_generation[w][h - 1]) {
						case forestfire::Field::Tree:
							trees++;
							break;
						case forestfire::Field::Burning:
							burning++;
							break;
						default:;
						}
					}

					// Neighbor North-East
					if (w + 1 < args->width_forest && h - 1 >= 0) {
						switch (args->curr_generation[w + 1][h - 1]) {
						case forestfire::Field::Tree:
							trees++;
							break;
						case forestfire::Field::Burning:
							burning++;
							break;
						default:;
						}
					}

					// Neighbor West
					if (w - 1 >= 0) {
						switch (args->curr_generation[w - 1][h]) {
						case forestfire::Field::Tree:
							trees++;
							break;
						case forestfire::Field::Burning:
							burning++;
							break;
						default:;
						}
					}

					// Neighbor East
					if (w + 1 < args->width_forest) {
						switch (args->curr_generation[w + 1][h]) {
						case forestfire::Field::Tree:
							trees++;
							break;
						case forestfire::Field::Burning:
							burning++;
							break;
						default:;
						}
					}

					// Neighbor South-West
					if (w - 1 >= 0 && h + 1 < args->height_forest) {
						switch (args->curr_generation[w - 1][h + 1]) {
						case forestfire::Field::Tree:
							trees++;
							break;
						case forestfire::Field::Burning:
							burning++;
							break;
						default:;
						}
					}

					// Neighbor South
					if (h + 1 < args->height_forest) {
						switch (args->curr_generation[w][h + 1]) {
						case forestfire::Field::Tree:
							trees++;
							break;
						case forestfire::Field::Burning:
							burning++;
							break;
						default:;
						}
					}

					// Neighbor South-East
					if (w + 1 < args->width_forest && h + 1 < args->height_forest) {
						switch (args->curr_generation[w + 1][h + 1]) {
						case forestfire::Field::Tree:
							trees++;
							break;
						case forestfire::Field::Burning:
							burning++;
							break;
						default:;
						}
					}

					// Define next_generation for current field
					switch (args->curr_generation[w][h]) {
					case forestfire::Field::Tree:
						// 1. A tree that is next to a burning tree will turn into a burning tree itself
						if (burning > 0) {
							args->next_generation[w][h] = forestfire::Field::Burning;
						}
						// 3. A tree with no burning neighbors will ignite with probability IGNITION
						//    as a result of a lightning strike.
						else {
							double ignition = random(generator);
							if (ignition < args->ignition) {
								args->next_generation[w][h] = forestfire::Field::Burning;
							}
							else {
								args->next_generation[w][h] = forestfire::Field::Tree;
							}
						}
						break;
					case forestfire::Field::Burning:
						// 2. A burning tree will turn into an empty space
						args->next_generation[w][h] = forestfire::Field::Empty;
						break;
					case forestfire::Field::Empty:
					{
					 	// 4. An empty space will turn into a tree with probability GROWTH * (trees + 1),
					 	//    where `trees` is the number of non-burning trees neighboring this space.
 						double growth = random(generator);
 						if (growth < (args->growth * (trees + 1))) {
 							args->next_generation[w][h] = forestfire::Field::Tree;
 						}
 						else {
	 						args->next_generation[w][h] = forestfire::Field::Empty;
 						}
 						break;
					}
					default:;
					}
				} // height loop
			} // width loop
			
			pthread_barrier_wait(&forest_thread::before_swap_barrier);

			// Every thread now can swap generations
			std::swap(args->curr_generation, args->next_generation);

			pthread_barrier_wait(&forest_thread::after_swap_barrier);
		} // generation loop

		return NULL;
	} // forestfire_pthread
} // namespace forest_thread

int main(int argc, char **argv)
{
	if (argc < 6) {
		std::cout << "Five parameters needed:" << std::endl;
		std::cout << "- Path to the text file" << std::endl;
		std::cout << "- Ignition probability (0..1)" << std::endl;
		std::cout << "- Growth probability (0..1)" << std::endl;
		std::cout << "- Number of generations (> 0)" << std::endl;
		std::cout << "- Number of threads (> 1)" << std::endl;
		return EXIT_FAILURE;
	}

	std::cout << "Given parameters:" << std::endl;
	for (int i = 0; i < argc; i++) {
		std::cout << i << ": " << argv[i] << std::endl;
	}

	const double IGNITION = std::stod(argv[2]);
	const double GROWTH = std::stod(argv[3]);
	const unsigned int NUM_GENERATIONS = std::stoi(argv[4]);
	const unsigned int NUM_THREADS = std::stoi(argv[5]);

	// Load initial forest from the text file
	std::string path = std::string(argv[1]);
	forestfire::Forest FOREST { 0, 0, forestfire::FieldVector() };
	forestfire::load_text_file(path, FOREST);
	std::cout << "Forest: [" << FOREST.width << "x" << FOREST.height << "]" << std::endl;

	const int WIDTH = FOREST.width;
	const int HEIGHT = FOREST.height;

	// Create an 2D-array of enum `Field` from FieldVector for better computation
	forestfire::Field **generation1 = new forestfire::Field *[WIDTH];
	forestfire::Field **generation2 = new forestfire::Field *[WIDTH];

	for (int i = 0; i < WIDTH; i++) {
		generation1[i] = new forestfire::Field[HEIGHT];
		generation2[i] = new forestfire::Field[HEIGHT];
	}

	forestfire::Field **curr_generation = generation1;
	forestfire::Field **next_generation = generation2;

	// Init current generation
	// Fill up next generation with empty space
	for (int h = 0; h < HEIGHT; h++) {
		for (int w = 0; w < WIDTH; w++) {
			curr_generation[w][h] = FOREST.fields[h * WIDTH + w];
			next_generation[w][h] = forestfire::Field::Empty;
		}
	}

	// POSIX Threads settings
	pthread_t *threads = new pthread_t[NUM_THREADS];
	struct forest_thread::args *args = new struct forest_thread::args[NUM_THREADS];

	// Calculate width and height intervals for every thread
	const unsigned int FIELDS = (unsigned int) (WIDTH * HEIGHT);
	const unsigned int FIELDS_PER_THREAD = (FIELDS % NUM_THREADS == 0) ? (FIELDS / NUM_THREADS) : (FIELDS / NUM_THREADS + 1);

	int width_from = 0;
	int height_from = 0;

	// Init argument struct for every thread
	for (unsigned int i = 0; i < NUM_THREADS; i++) {
		args[i].curr_generation = curr_generation;
		args[i].next_generation = next_generation;
		args[i].width_forest = WIDTH;
		args[i].height_forest = HEIGHT;
		args[i].ignition = IGNITION;
		args[i].growth = GROWTH;
		args[i].width_from = width_from;
		args[i].height_from = height_from;
		// Last thread gets the rest of the fields, which might be less than FIELDS_PER_THREAD
		args[i].num_fields = (i != NUM_THREADS - 1) ? FIELDS_PER_THREAD : (FIELDS - FIELDS_PER_THREAD * (NUM_THREADS - 1));
		args[i].num_generations = NUM_GENERATIONS;

		width_from = (width_from + FIELDS_PER_THREAD) % WIDTH;
		height_from = (FIELDS_PER_THREAD * (i + 1)) / WIDTH;
	}

	// Barriers init
	pthread_barrier_init(&forest_thread::start_barrier, NULL, NUM_THREADS);
	pthread_barrier_init(&forest_thread::before_swap_barrier, NULL, NUM_THREADS);
	pthread_barrier_init(&forest_thread::after_swap_barrier, NULL, NUM_THREADS);

	//------------------------------START------------------------------
	std::cout << "Calculating..." << std::endl;
	auto start_time = std::chrono::high_resolution_clock::now();

	// Threads Start
	for (unsigned int i = 0; i < NUM_THREADS; i++) {
		pthread_create(&threads[i], NULL, forest_thread::forestfire_pthread, &args[i]);
	}

	// Threads joining
	for (unsigned int i = 0; i < NUM_THREADS; i++) {
		pthread_join(threads[i], NULL);
	}

	//-------------------------------END-------------------------------
	// Calculate and print execution time
	auto end_time = std::chrono::high_resolution_clock::now();
	auto duration_milisec = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count();
	auto duration_seconds = std::chrono::duration_cast<std::chrono::seconds>(end_time - start_time).count();
	std::cout << "---------------------------------------" << std::endl;
	std::cout << "Duration: " << std::endl;
	std::cout << duration_milisec << " ms" << std::endl;
	std::cout << duration_seconds << " s" << std::endl;

	// Barriers destroy
	pthread_barrier_destroy(&forest_thread::start_barrier);
	pthread_barrier_destroy(&forest_thread::before_swap_barrier);
	pthread_barrier_destroy(&forest_thread::after_swap_barrier);

	// Generate new path for result file
	std::string new_file_name = forestfire::generate_result_path(path, NUM_GENERATIONS);
	// Write solution to a file
	forestfire::write_text_file(new_file_name, curr_generation, WIDTH, HEIGHT);

	return EXIT_SUCCESS;
}

# Forest Fire Simulation
Several simple programs to simulate a forest fire for a given input file
and its respectful parameters like _ignition probability_, _growth probability_
and _number of generations_ to simulate.

## Input file structure
Contains three possible characters:
* **T** represents a living tree
* **X** represents a burning tree
* **&lt;space&gt;** represents an empty spot

New row begins with a new line.

### Example
```txt
TT XT
TXXT 
TTT T
```

# Project structure
Containing five folders:
* **forestfire:** Static library for reading and writing the text file
* **cuda:** CUDA Implementation for the problem
* **openmp:** OpemMP Implementation for the fire simulation
* **pthreads:** Implementation with pthreads for parallelization
* **sequential:** Sequential implementation
* **input_data:** Example input files for testing

# Building
1. Create build directory `mkdir build`
2. Switch to the build folder `cd build`
3. Run CMake to generate build files: `cmake ..`
	* `-G<string>` determines what kind of build files are going to be generated
	* Documentation about [CMake Generators](https://cmake.org/cmake/help/v3.4/manual/cmake-generators.7.html)

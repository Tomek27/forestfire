// forestfireSequential.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
// @author: Tomasz Brewka

#include <chrono>
#include <iostream>
#include <random>

#include <omp.h>

#include "forestfire.h"

int main(int argc, char **argv)
{
	if (argc < 5) {
		std::cout << "Five parameters needed:" << std::endl;
		std::cout << "- Path to the text file" << std::endl;
		std::cout << "- Ignition probability (0..1)" << std::endl;
		std::cout << "- Growth probability (0..1)" << std::endl;
		std::cout << "- Number of generations (> 0)" << std::endl;
		std::cout << "OpenMP sets the number of threads depending on the for loops" << std::endl;
		return EXIT_FAILURE;
	}

	std::cout << "Given parameters:" << std::endl;
	for (int i = 0; i < argc; i++) {
		std::cout << i << ": " << argv[i] << std::endl;
	}

	const double IGNITION = std::stod(argv[2]);
	const double GROWTH = std::stod(argv[3]);
	const unsigned int NUM_GENERATIONS = std::stoi(argv[4]);

	// Load initial forest from the text file
	std::string path = std::string(argv[1]);
	forestfire::Forest FOREST { 0, 0, forestfire::FieldVector() };
	forestfire::load_text_file(path, FOREST);
	std::cout << "Forest: [" << FOREST.width << "x" << FOREST.height << "]" << std::endl;

	const int WIDTH = FOREST.width;
	const int HEIGHT = FOREST.height;

	// Create an 2D-array of enum `Field` from FieldVector for better computation
	forestfire::Field **generation1 = new forestfire::Field *[WIDTH];
	forestfire::Field **generation2 = new forestfire::Field *[WIDTH];

	for (int i = 0; i < WIDTH; i++) {
		generation1[i] = new forestfire::Field[HEIGHT];
		generation2[i] = new forestfire::Field[HEIGHT];
	}

	forestfire::Field **curr_generation = generation1;
	forestfire::Field **next_generation = generation2;

	// Init current generation
	for (int h = 0; h < HEIGHT; h++) {
		for (int w = 0; w < WIDTH; w++) {
			curr_generation[w][h] = FOREST.fields[h * WIDTH + w];
			next_generation[w][h] = forestfire::Field::Empty;
		}
	}

	// Random settings
	std::random_device rd;  // Will be used to obtain a seed for the random number engine
	std::mt19937 generator(rd()); // Standard mersenne_twister_engine seeded with rd()
	std::uniform_real_distribution<> random(forestfire::RANDOM_MIN, forestfire::RANDOM_MAX);

	//------------------------------START------------------------------
	std::cout << "Calculating..." << std::endl;
	auto start_time = std::chrono::high_resolution_clock::now();

	for (unsigned int generation = 1; generation <= NUM_GENERATIONS; generation++) {

		#pragma omp parallel for
		for (int w = 0; w < WIDTH; w++) {

			#pragma omp parallel for
			for (int h = 0; h < HEIGHT; h++) {

				unsigned int trees = 0;
				unsigned int burning = 0;

				// Get neighbors of current field
				// Count burning and not burning trees
				// +-------+
				// |NW|N|NE|
				// +--+-+--+
				// |W |.| E|
				// +--+-+--+
				// |SW|S|SE|
				// +-------+

				// Neighbor North-West
				if (w - 1 >= 0 && h - 1 >= 0) {
					switch (curr_generation[w - 1][h - 1]) {
					case forestfire::Field::Tree:
						trees++;
						break;
					case forestfire::Field::Burning:
						burning++;
						break;
					default:;
					}
				}

				// Neighbor North
				if (h - 1 >= 0) {
					switch (curr_generation[w][h - 1]) {
					case forestfire::Field::Tree:
						trees++;
						break;
					case forestfire::Field::Burning:
						burning++;
						break;
					default:;
					}
				}

				// Neighbor North-East
				if (w + 1 < WIDTH && h - 1 >= 0) {
					switch (curr_generation[w + 1][h - 1]) {
					case forestfire::Field::Tree:
						trees++;
						break;
					case forestfire::Field::Burning:
						burning++;
						break;
					default:;
					}
				}

				// Neighbor West
				if (w - 1 >= 0) {
					switch (curr_generation[w - 1][h]) {
					case forestfire::Field::Tree:
						trees++;
						break;
					case forestfire::Field::Burning:
						burning++;
						break;
					default:;
					}
				}

				// Neighbor East
				if (w + 1 < WIDTH) {
					switch (curr_generation[w + 1][h]) {
					case forestfire::Field::Tree:
						trees++;
						break;
					case forestfire::Field::Burning:
						burning++;
						break;
					default:;
					}
				}

				// Neighbor South-West
				if (w - 1 >= 0 && h + 1 < HEIGHT) {
					switch (curr_generation[w - 1][h + 1]) {
					case forestfire::Field::Tree:
						trees++;
						break;
					case forestfire::Field::Burning:
						burning++;
						break;
					default:;
					}
				}

				// Neighbor South
				if (h + 1 < HEIGHT) {
					switch (curr_generation[w][h + 1]) {
					case forestfire::Field::Tree:
						trees++;
						break;
					case forestfire::Field::Burning:
						burning++;
						break;
					default:;
					}
				}

				// Neighbor South-East
				if (w + 1 < WIDTH && h + 1 < HEIGHT) {
					switch (curr_generation[w + 1][h + 1]) {
					case forestfire::Field::Tree:
						trees++;
						break;
					case forestfire::Field::Burning:
						burning++;
						break;
					default:;
					}
				}

				// Define next_generation for current field
				switch (curr_generation[w][h]) {
				case forestfire::Field::Tree:
					// 1. A tree that is next to a burning tree will turn into a burning tree itself
					if (burning > 0) {
						next_generation[w][h] = forestfire::Field::Burning;
					}
					// 3. A tree with no burning neighbors will ignite with probability IGNITION
					//    as a result of a lightning strike.
					else {
						double ignition = random(generator);
						if (ignition < IGNITION) {
							next_generation[w][h] = forestfire::Field::Burning;
						}
						else {
							next_generation[w][h] = forestfire::Field::Tree;
						}
					}
					break;
				case forestfire::Field::Burning:
					// 2. A burning tree will turn into an empty space
					next_generation[w][h] = forestfire::Field::Empty;
					break;
				case forestfire::Field::Empty:
				{
					// 4. An empty space will turn into a tree with probability GROWTH * (trees + 1),
					//    where `trees` is the number of non-burning trees neighboring this space.
					double growth = random(generator);
					if (growth < (GROWTH * (trees + 1))) {
						next_generation[w][h] = forestfire::Field::Tree;
					}
					else {
						next_generation[w][h] = forestfire::Field::Empty;
					}
					break;
				}
				default:;
				}
			} // height loop
		} // width loop
		// Swap generations
		std::swap(curr_generation, next_generation);
	} // generation loop

	//-------------------------------END-------------------------------
	// Calculate and print execution time
	auto end_time = std::chrono::high_resolution_clock::now();
	auto duration_milisec = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count();
	auto duration_seconds = std::chrono::duration_cast<std::chrono::seconds>(end_time - start_time).count();
	std::cout << "---------------------------------------" << std::endl;
	std::cout << "Duration: " << std::endl;
	std::cout << duration_milisec << " ms" << std::endl;
	std::cout << duration_seconds << " s" << std::endl;

	// Generate new path for result file
	std::string new_file_name = forestfire::generate_result_path(path, NUM_GENERATIONS);
	// Write solution to a file
	forestfire::write_text_file(new_file_name, curr_generation, WIDTH, HEIGHT);

	return EXIT_SUCCESS;
}
